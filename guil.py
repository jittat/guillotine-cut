
X1 = 0
Y1 = 1
X2 = 2
Y2 = 3

def read_input():
    n = int(input())
    rects = []
    for i in range(n):
        x1,y1,x2,y2 = [int(x) for x in input().split()]
        rects.append((x1,y1,x2,y2))

    return rects

def get_xcors(rects):
    return sorted(list(set([r[X1] for r in rects] + [r[X2] for r in rects])))
    
def get_ycors(rects):
    return sorted(list(set([r[Y1] for r in rects] + [r[Y2] for r in rects])))
    

def preprocess(rects):
    xcors = get_xcors(rects)
    ycors = get_ycors(rects)

    xmap = dict([(xcors[i],i) for i in range(len(xcors))])
    ymap = dict([(ycors[i],i) for i in range(len(ycors))])

    return [(xmap[r[X1]],
             ymap[r[Y1]],
             xmap[r[X2]],
             ymap[r[Y2]]) for r in rects], len(xcors)-1, len(ycors)-1

opt = {}

def solve(x1,y1,x2,y2,rects):
    if (x1,y1,x2,y2) in opt:
        return opt[(x1,y1,x2,y2)][0]

    if len(rects) <= 1:
        opt[(x1,y1,x2,y2)] = (len(rects),-1)
        return opt[(x1,y1,x2,y2)][0]

    bestx = 0
    bestxcut = 0
    xs = get_xcors(rects)
    for x in xs[1:-1]:
        rl = [r for r in rects if r[X2] <= x]
        rr = [r for r in rects if r[X1] >= x]

        optl = solve(x1,y1,x,y2,rl)
        optr = solve(x,y1,x2,y2,rr)

        if optl + optr > bestx:
            bestx = optl+ optr
            bestxcut = x
    
    besty = 0
    bestycut = 0
    ys = get_ycors(rects)
    for y in ys[1:-1]:
        rt = [r for r in rects if r[Y1] >= y]
        rb = [r for r in rects if r[Y2] <= y]

        optt = solve(x1,y,x2,y2,rt)
        optb = solve(x1,y1,x2,y,rb)

        if optt + optb > besty:
            besty = optt + optb
            bestycut = y

    if bestx > besty:
        opt[(x1,y1,x2,y2)] = (bestx, (0, bestxcut))
    else:
        opt[(x1,y1,x2,y2)] = (besty, (1, bestycut))
        
    return opt[(x1,y1,x2,y2)][0]

def trace_cut(x1,y1,x2,y2,depth):
    if opt[(x1,y1,x2,y2)][0] <= 1:
        return

    indent = '.' * depth
    
    direction, pos = opt[(x1,y1,x2,y2)][1]
    if direction == 0:
        print(f'{indent}-X:{pos}')
        trace_cut(x1,y1,pos,y2,depth+1)
        trace_cut(pos,y1,x2,y2,depth+1)
    else:
        print(f'{indent}-Y:{pos}')
        trace_cut(x1,y1,x2,pos,depth+1)
        trace_cut(x1,pos,x2,y2,depth+1)


def main():
    #rects, mx, my = preprocess(read_input())
    rects = read_input()
    n = len(rects)
    mx = get_xcors(rects)[-1]
    my = get_ycors(rects)[-1]

    print(solve(0,0,mx,my,rects))

    trace_cut(0,0,mx,my,0)
    
if __name__ == '__main__':
    main()
    
